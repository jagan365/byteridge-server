const userService = require('../users/user.service');
async function verifyRole (req, res, next) {
    try {
        console.log(`req.user.sub`,req.user.sub)
        const user = await userService.getById(req.user.sub);
        if(user.role && user.role === `AUDITOR`){
            next();
        }else{
            return res.status(401).json({})
        }
    } catch (error) {
      console.log(error)
      return res.status(500).json({ success: false, error: 'internal server error' })
    }
} 
module.exports = verifyRole
  