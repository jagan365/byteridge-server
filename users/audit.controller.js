const express = require('express');
const router = express.Router();
const userService = require('./user.service');
const verify = require('../_helpers/validateRole');

// routes

router.get('/audit', verify, getAll);

module.exports = router;

function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}